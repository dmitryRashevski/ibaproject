import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("fxml/root.fxml"));
        Pane root = loader.load();

        primaryStage.setTitle("WindowTitle");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
